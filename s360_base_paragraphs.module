<?php

/**
 * @file
 * s360_base_paragraphs.module
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Views;
use Drupal\webform\Entity\Webform;

/**
 * Implements hook_help().
 */
function s360_base_paragraphs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.s360_base_paragraphs':
      $paragraphs = [
        'cta_link' => t('CTA Link'),
        'curated_content' => t('Curated Content'),
        'document_list' => t('Document List'),
        'embed_code' => t('Embed Code'),
        'faq' => t('FAQ'),
        'html_content' => t('HTML Content'),
        'image' => t('Image'),
        'in_this_section' => t('In this Section'),
        'link_list' => t('Link List'),
        'placeholder' => t('Placeholder'),
        'video' => t('Video'),
        'view_block' => t('View Block'),
        'webform' => t('Webform'),
      ];

      $output = '';

      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module adds commonly used paragraphs types.') . '</p>';

      $output .= '<p>&nbsp;</p>';

      $output .= '<h3>' . t('Paragraph types') . '</h3>';
      $output .= '<dl>';

      foreach ($paragraphs as $paragraph_key => $paragraph_label) {
        $paragraph_config = \Drupal::config("paragraphs.paragraphs_type.$paragraph_key");

        // The paragraph is still configured (not deleted).
        if (!empty($paragraph_config->getRawData())) {
          $output .= '<dt><strong>' . $paragraph_config->get('label') . '</strong></dt>';
          $output .= '<dd>' . $paragraph_config->get('description') . '</dd>';
        }
        // The paragraph was deleted.
        else {
          $output .= "<dt><strong>$paragraph_label</strong></dt>";
          $output .= "<dd>This paragraph was removed.</dd>";
        }
      }

      $output .= '</dl>';

      return $output;
  }
}

/**
 * Implements hook_preprocess_paragraph() for view_block.
 */
function s360_base_paragraphs_preprocess_paragraph__view_block(&$variables) {
  if (_s360_base_paragraphs_is_admin_route()) {
    /** @var \Drupal\paragraph\Entity\Paragraph $paragraph */
    $paragraph = $variables['paragraph'];

    if ($paragraph->hasField('field_view') && $paragraph->get('field_view')->count()) {
      $field_view = $paragraph->get('field_view')->first()->getValue();
      $view_target_id = $field_view['target_id'];
      $view_display_id = $field_view['display_id'];

      /** @var \Drupal\views\ViewExecutable $view */
      $view = Views::getView($view_target_id);

      if ($view) {
        $view->setDisplay($view_display_id);
        $display_obj = $view->getDisplay();
        $display_name = $display_obj->display['display_title'];
        $field_item_text = $view->storage->label() . ' (' . $display_name . ')';
      }
      else {
        $field_item_text = 'Problem loading the view.';
      }

      $variables['content']['field_view'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            'field',
            'field--name-field-view',
            'field--type-string',
            'field--label-inline',
          ],
        ],
        'child' => [
          [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => 'View',
            '#attributes' => [
              'class' => 'field__label',
            ],
          ],
          [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => $field_item_text,
            '#attributes' => [
              'class' => 'field__item',
            ],
          ],
        ],
      ];
    }
  }
}

/**
 * Implements hook_preprocess_paragraph() for webform.
 */
function s360_base_paragraphs_preprocess_paragraph__webform(&$variables) {
  if (_s360_base_paragraphs_is_admin_route()) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $variables['paragraph'];

    if ($paragraph->hasField('field_webform') && $paragraph->get('field_webform')->count()) {
      $field_webform = $paragraph->get('field_webform')->first()->getValue();
      $wf_target_id = $field_webform['target_id'];

      /** @var \Drupal\webform\Entity\Webform $wf */
      $wf = Webform::load($wf_target_id);

      if ($wf) {
        $field_item_text = $wf->label() . ($wf->getDescription()
          ? '<br> ' . $wf->getDescription()
          : '');
      }
      else {
        $field_item_text = 'Problem loading the webform.';
      }

      $variables['content']['field_webform'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            'field',
            'field--name-field-webform',
            'field--type-string',
            'field--label-inline',
          ],
        ],
        'child' => [
          [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => 'Webform',
            '#attributes' => [
              'class' => 'field__label',
            ],
          ],
          [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => $field_item_text,
            '#attributes' => [
              'class' => 'field__item',
            ],
          ],
        ],
      ];
    }
  }
}

/**
 * Checks the route name to see if it's an "admin route".
 *
 * @return bool
 *   Returns true if the current route is found in the array, false otherwise.
 */
function _s360_base_paragraphs_is_admin_route() {
  return in_array(Drupal::routeMatch()->getRouteName(), [
    'node.add',
    'entity.node.edit_form',
    'entity.group.edit_form',
    'layout_paragraphs.builder.edit_item',
    'layout_paragraphs.builder.insert',
  ]);
}
